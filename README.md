## aoc - Advent of Code boiler plate

## Description
aoc generates boiler plate code in go to make it easier to solve the Advent of Code puzzles. The puzzle descriptions are downloaded and converted to markdown. Also the puzzle input is saved to input.txt. The generated go code provides a -submit flag to auto submit your answer to the puzzle.

<!-- ## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge. -->

## Visuals
### Installation
![go install mrvantage/aoc/cmd/aoc](examples/install.svg)

### Login
![aoc login -session-id secret](examples/login.svg)

### Generate
![aoc generate -event 2015 -day 1](examples/generate.svg)

### Submit
![go run event_2015_day_1.go -submit](examples/submit.svg)
## Installation
```
go install gitlab.com/mrvantage/aoc/cmd/aoc
```
## Usage

### Login
To be able to submit your answers and retrieve part two of the daily assignments you need to be somehow authenticated. For this you can steal the session cookie from your browser when logged in to Advent of Code. You can then set the session cookie using the following command:
```
aoc login -session-id my_super_secret_session_id_stolen_from_browser
```

### Generating
To retrieve a puzzle from AoC and create the boiler plate code for it run:
```
aoc generate -event 2021 -day 5
```
The boiler plate will then be created in `tasks/2021/5` in your current working dir

### Implementing the solution to the puzzle
Now you can start to code the solution to todays puzzle in the generated go file. If you run the file the code for the first part will be executed by default
```
cd tasks/2021/5
go run event_2021_day_5.go
```

If you're working on part 2, you can pass the `-part 2` flag
```
go run event_2021_day_5.go -part 2
```

### Submitting answer to Advent of Code
Once you have implemented the solution to the puzzle and are confident your solution is correct you can have the code submit the answer by passing the `-submit` flag
```
go run event_2021_day_5.go -submit
```