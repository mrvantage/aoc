package aoc

import (
	"io"
	"os"
	"path/filepath"

	yaml "gopkg.in/yaml.v3"
)

type Config struct {
	file    string
	Session string `yaml:"session,omitempty"`
}

func (c *Config) Load() error {
	f, err := os.Open(c.file)
	if err != nil {
		return err
	}
	b, err := io.ReadAll(f)
	if err != nil {
		return err
	}
	return yaml.Unmarshal(b, c)
}

func (c *Config) Save() error {
	raw, err := yaml.Marshal(c)
	if err != nil {
		return err
	}
	err = createDirSubTree(filepath.Dir(c.file), 0755)
	if err != nil {
		return err
	}
	f, err := os.Create(c.file)
	if err != nil {
		return err
	}
	_, err = f.Write(raw)
	return err
}

func NewConfigFromFile(file string) (*Config, error) {
	return &Config{
		file: file,
	}, nil
}

func NewConfig() (*Config, error) {
	home, err := os.UserHomeDir()
	if err != nil {
		return nil, err
	}
	return NewConfigFromFile(home + "/.config/aoc/config.yml")
}
