package aoc

import (
	"errors"
	"fmt"
	"io/fs"
	"log"
	"os"
	"path/filepath"
	"runtime"
)

type Manager struct {
	cache  *Cache
	config *Config
}

func (m Manager) GetTask(event, day int) (*Task, error) {
	t := m.cache.GetTask(event, day)
	if t != nil {
		return t, nil
	}

	return m.GetTaskHTTP(event, day)
}

func (m Manager) GetTaskHTTP(event, day int) (*Task, error) {

	t := NewTask(event, day)
	err := t.Retrieve(m.config.Session)
	if err != nil {
		return nil, err
	}

	err = t.RetrieveInput(m.config.Session)
	if err != nil {
		return nil, err
	}

	err = m.cache.AddOrUpdateTask(t)
	if err != nil {
		return nil, err
	}

	return t, nil
}

func (m Manager) RenderBoilerplate(basePath string, task *Task) error {

	if basePath == "" {
		basePath = filepath.Join(".", "tasks", fmt.Sprintf("%d", task.Event), fmt.Sprintf("%d", task.Day))
	}

	md, err := task.GetTaskArticlesMd()
	if err != nil {
		return err
	}

	for num, part := range md {
		partNum := num + 1
		filePath := filepath.Join(basePath, fmt.Sprintf("part_%d.md", partNum))
		err = createFileAndWriteString(filePath, part)
		if err != nil {
			return err
		}
	}

	src, err := task.RenderTemplate()
	if err != nil {
		return err
	}

	filePath := filepath.Join(basePath, fmt.Sprintf("event_%d_day_%d.go", task.Event, task.Day))
	err = createFileAndWriteString(filePath, src)
	if err != nil {
		return err
	}

	filePath = filepath.Join(basePath, "input.txt")
	return createFileAndWriteString(filePath, task.Input)
}

func (m Manager) SubmitResult(task *Task, part, answer int) (*AnswerFeedback, error) {
	feedback, err := task.SubmitAnswer(m.config.Session, part, answer)
	if err != nil {
		return nil, err
	}

	// If we're submitting the first part, refresh the task in cache, just in case we've submitted the correct answer
	// and have unlocked the second part.
	if part == 1 {
		err = task.Retrieve(m.config.Session)
		if err != nil {
			return nil, err
		}

		err = m.cache.AddOrUpdateTask(task)
		if err != nil {
			return nil, err
		}

		m.RenderBoilerplate(m.GetCallerPath(1), task)
	}

	return feedback, nil
}

func (m Manager) GetCallerPath(skip int) string {
	// Add one to skip to compensate for this method, so that the caller can
	// use skip = 0 to get the path of itself.
	skip++
	_, caller, _, _ := runtime.Caller(skip)
	return filepath.Dir(caller)
}

func (m *Manager) SetSessionID(session string) error {
	m.config.Session = session
	return m.config.Save()
}

func NewManager() (*Manager, error) {
	cache, err := NewCache()
	if err != nil {
		return nil, err
	}

	cfg, err := NewConfig()
	if err != nil {
		return nil, err
	}

	err = cfg.Load()
	if err != nil {
		log.Printf("warning: unable to load config - %s", err.Error())
	}

	return &Manager{
		cache:  cache,
		config: cfg,
	}, nil
}

func createFileAndWriteString(path, content string) error {
	f, err := createFileWithPath(path, 0775)
	if err != nil {
		return err
	}
	defer f.Close()

	if f != nil {
		log.Printf("info: writing file %s", path)
		_, err = f.WriteString(content)
		if err != nil {
			return err
		}
	}
	return nil
}

func createFileWithPath(path string, mode fs.FileMode) (*os.File, error) {
	err := createDirSubTree(filepath.Dir(path), mode)
	if err != nil {
		return nil, err
	}

	_, err = os.Stat(path)
	if err == nil {
		return nil, nil
	} else if errors.Is(err, os.ErrNotExist) {
		log.Printf("info: creating file %s", path)
		return os.Create(path)
	}
	return nil, err
}

func createDirSubTree(path string, mode fs.FileMode) error {

	// Check if the directory we are trying to create does not exist yet.
	// If it does, and is a directory nothing has to be done, that was easy!
	// If it exists and is NOT a directory, we'll return an error.
	// Any other errors we get, except for the ErrNotExist can be returned as is.
	s, err := os.Stat(path)
	if err == nil {
		if s.IsDir() {
			return nil
		} else {
			return fmt.Errorf("path %s exists, but is not a directory", path)
		}
	} else if !errors.Is(err, os.ErrNotExist) {
		log.Printf("error: stat failed - %s", err.Error())
		log.Printf("%#v", err)
		return err
	}

	// The dir seems to be non existing, so let's create it.
	// First call createDirSubTree on the parent dir recursively to make sure it also exists.
	parent := filepath.Dir(path)
	err = createDirSubTree(parent, mode)
	if err != nil {
		log.Printf("error: recursive call failed - %s", err.Error())
		return err
	}

	// Finally everything looks good to try to create the directory.
	log.Printf("info: creating directory %s", path)
	err = os.Mkdir(path, mode)
	if err != nil {
		log.Printf("error: mkdir failed - %s", err.Error())
		return err
	}

	return nil
}
