package aoc

import (
	"bytes"
	"embed"
	"fmt"
	"io"
	"log"
	"net/http"
	"net/url"
	"strings"
	"text/template"
	"time"

	md "github.com/JohannesKaufmann/html-to-markdown"
	"github.com/antchfx/htmlquery"
	"golang.org/x/net/html"
)

var (
	//go:embed templates/task.go.tpl
	embedFS embed.FS
)

type AnswerFeedback struct {
	Time time.Time
	Body string
}

func (r AnswerFeedback) GetArticleMd() (string, error) {

	articles, err := getArticles(r.Body)
	if err != nil {
		return "", err
	}

	if len(articles) > 0 {
		md, err := renderHTMLNodeMarkdown(articles[0])
		if err != nil {
			return "", err
		}
		return md, nil
	}

	return "", fmt.Errorf("no article found in result")
}

func (r AnswerFeedback) String() string {
	s, _ := r.GetArticleMd()
	return s
}

func NewAnswerFeedback(body string) *AnswerFeedback {
	return &AnswerFeedback{
		Time: time.Now(),
		Body: body,
	}
}

type Task struct {
	Event    int
	Day      int
	Feedback map[int][]*AnswerFeedback
	TaskBody string
	Input    string
}

func (t *Task) Retrieve(session string) error {
	log.Printf("info: retrieving task for event %d day %d", t.Event, t.Day)

	var err error
	req, err := http.NewRequest("GET", fmt.Sprintf("https://adventofcode.com/%d/day/%d", t.Event, t.Day), nil)
	if err != nil {
		return err
	}

	if len(session) > 0 {
		req.AddCookie(&http.Cookie{
			Name:  "session",
			Value: session,
		})
	}

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return err
	}
	defer res.Body.Close()

	body, err := io.ReadAll(res.Body)
	if err != nil {
		return err
	}

	t.TaskBody = string(body)

	return nil
}

func (t *Task) RetrieveInput(session string) error {
	log.Printf("info: retrieving task input for event %d day %d", t.Event, t.Day)

	var err error
	req, err := http.NewRequest("GET", fmt.Sprintf("https://adventofcode.com/%d/day/%d/input", t.Event, t.Day), nil)
	if err != nil {
		return err
	}

	if len(session) > 0 {
		req.AddCookie(&http.Cookie{
			Name:  "session",
			Value: session,
		})
	}

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return err
	}
	defer res.Body.Close()

	body, err := io.ReadAll(res.Body)
	if err != nil {
		return err
	}

	t.Input = string(body)

	return nil
}

func (t *Task) SubmitAnswer(session string, part, answer int) (*AnswerFeedback, error) {

	if len(session) == 0 {
		return nil, fmt.Errorf("a valid session id is required to submit results")
	}

	log.Printf("info: submitting task answer for event %d day %d", t.Event, t.Day)

	data := url.Values{
		"level":  {fmt.Sprintf("%d", part)},
		"answer": {fmt.Sprintf("%d", answer)},
	}

	var err error
	req, err := http.NewRequest("POST", fmt.Sprintf("https://adventofcode.com/%d/day/%d/answer", t.Event, t.Day), strings.NewReader(data.Encode()))
	if err != nil {
		return nil, err
	}

	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")

	if len(session) > 0 {
		req.AddCookie(&http.Cookie{
			Name:  "session",
			Value: session,
		})
	}

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()

	body, err := io.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}

	r := NewAnswerFeedback(string(body))
	t.AddResult(part, r)

	return r, nil
}

func (t Task) GetTaskArticlesMd() ([]string, error) {
	result := make([]string, 0)
	articles, err := getArticles(t.TaskBody)
	if err != nil {
		return result, err
	}

	for _, article := range articles {
		md, err := renderHTMLNodeMarkdown(article)
		if err != nil {
			return result, err
		}
		result = append(result, md)
	}
	return result, err
}

func (t Task) RenderTemplate() (string, error) {
	tpl, err := template.ParseFS(embedFS, "templates/task.go.tpl")
	if err != nil {
		return "", err
	}

	data := struct {
		Event int
		Day   int
	}{
		Event: t.Event,
		Day:   t.Day,
	}

	var b bytes.Buffer
	err = tpl.Execute(&b, data)
	if err != nil {
		return "", err
	}
	return b.String(), nil
}

func (t *Task) AddResult(part int, result *AnswerFeedback) {
	if t.Feedback[part] == nil {
		t.Feedback[part] = make([]*AnswerFeedback, 0)
	}
	t.Feedback[part] = append(t.Feedback[part], result)
}

func NewTask(event, day int) *Task {
	partResults := make(map[int][]*AnswerFeedback, 0)
	return &Task{
		Event:    event,
		Day:      day,
		Feedback: partResults,
	}
}

func getArticles(input string) ([]*html.Node, error) {
	doc, err := htmlquery.Parse(strings.NewReader(input))
	return htmlquery.Find(doc, "//article"), err
}

func renderHTMLNodeSource(node *html.Node) (string, error) {
	var b bytes.Buffer
	err := html.Render(&b, node)
	return b.String(), err
}

func renderHTMLNodeMarkdown(node *html.Node) (string, error) {
	src, err := renderHTMLNodeSource(node)
	if err != nil {
		return "", err
	}

	c := md.NewConverter("", true, nil)
	result, err := c.ConvertString(src)
	if err != nil {
		return "", err
	}

	return result, nil
}
