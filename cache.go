package aoc

import (
	"encoding/gob"
	"errors"
	"os"
	"path/filepath"
)

type Cache struct {
	Tasks map[int]map[int]*Task
	File  string
}

func (c Cache) fileExists() bool {
	_, err := os.Stat(c.File)
	return !errors.Is(err, os.ErrNotExist)
}

func (c Cache) SaveToFile() error {

	err := createDirSubTree(filepath.Dir(c.File), 0755)
	if err != nil {
		return err
	}

	f, err := os.Create(c.File)
	if err != nil {
		return err
	}
	e := gob.NewEncoder(f)
	return e.Encode(c)
}

func (c *Cache) LoadFromFile() error {
	f, err := os.Open(c.File)
	if err != nil {
		return err
	}
	d := gob.NewDecoder(f)
	return d.Decode(c)
}

func (c *Cache) AddOrUpdateTask(task *Task) error {

	if _, eventExists := c.Tasks[task.Event]; !eventExists {
		event := make(map[int]*Task, 0)
		c.Tasks[task.Event] = event
	}

	c.Tasks[task.Event][task.Day] = task

	return c.SaveToFile()
}

func (c Cache) GetTask(event, day int) *Task {
	if _, eventExists := c.Tasks[event]; eventExists {
		if task, dayExists := c.Tasks[event][day]; dayExists {
			return task
		}
	}
	return nil
}

func NewCache() (*Cache, error) {
	home, err := os.UserHomeDir()
	if err != nil {
		return nil, err
	}

	return NewCacheWithFile(home + "/.config/aoc/cache.bin")
}

func NewCacheWithFile(file string) (*Cache, error) {
	tasks := make(map[int]map[int]*Task, 0)
	cache := &Cache{
		Tasks: tasks,
		File:  file,
	}
	var err error
	if cache.fileExists() {
		err = cache.LoadFromFile()
	}
	return cache, err
}
