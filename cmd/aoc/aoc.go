package main

import (
	"flag"
	"fmt"
	"log"
	"os"

	"gitlab.com/mrvantage/aoc"
)

func main() {

	action := "unknown"
	if len(os.Args) > 1 {
		action = os.Args[1]
		os.Args = os.Args[1:]
	}

	mgr, err := aoc.NewManager()
	if err != nil {
		log.Fatalf("fatal: unable to create manager %s", err.Error())
	}

	switch action {
	case "generate":
		event := flag.Int("event", 2021, "Event (year of AoC edition)")
		day := flag.Int("day", 1, "Day of the event")
		flag.Parse()

		t, err := mgr.GetTask(*event, *day)
		if err != nil {
			log.Fatalf("fatal: error getting task %s", err.Error())
		}

		err = mgr.RenderBoilerplate("", t)
		if err != nil {
			log.Fatalf("fatal: unable to generate boilerplate - %s", err.Error())
		}
	case "login":
		session := flag.String("session-id", "", "Session id - steal it from the cookie in your browser ;-)")
		flag.Parse()
		err := mgr.SetSessionID(*session)
		if err != nil {
			log.Fatalf("fatal: unable to set session id - %s", err.Error())
		}
	default:
		fmt.Printf("No such action `%s`. Please use one of supported actions: `generate`, `login`\n", action)
	}

}
